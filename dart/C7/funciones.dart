import 'dart:io';

main() {
  stdout.write('Ingresa tu nombre: ');
  String nombre = stdin.readLineSync() ?? '';
  saludo(nombre);
}

void saludo(String nombre) {
  stdout.writeln('Hola $nombre');
}
