/*
  TAREA
  ------------------------------------------------
  1. recibir los siguientes datos de 'n' personas:
    > nombre, edad, sexo, país, y teléfono

  2. considerar las siguientes reglas:
    > las personas que sean mayores de edad se deben 
      guardar en una lista para mayores de edad.
    > las personas que sean menores de edad se deben
      guardar en una lista para menores de edad.
    > las personas que sean menores o igual a 2 años
      se deben guardar en una lista para bebes.
    > las personas que sean mayores o igual a 60 años
      se deben guardar en una lista para ancianos.

  3. imprimir ¿cual es la persona de menor edad?,
    ¿cual es la persona de mayor edad? y
    ¿cuantos años hay de diferencia entre ambos?
  ------------------------------------------------
*/

import 'dart:io';

// definición de las listas que contendran la info de personas
List<Map<String, dynamic>> bebes = [],
    menores = [],
    mayores = [],
    ancianos = [];
Map<String, dynamic> menor = {}, mayor = {};

void imprimir_cabecera_captura(int contador, int cantidad) {
  stdout.writeln('CAPTURA DE PERSONA: $contador de $cantidad');
  stdout.writeln('--------------------------------------------------');
}

String capturar_cadena(String campo, [String predeterminado = '']) {
  stdout.write('$campo: ');
  String valor = stdin.readLineSync() ?? '';

  if (valor.length == 0) valor = predeterminado;
  return valor;
}

int capturar_numero(String campo) {
  String valor = capturar_cadena(campo, '0');
  return int.parse(valor);
}

// función para capturar la información de la persona
Map<String, dynamic> capturar_datos(int contador, int cantidad) {
  imprimir_cabecera_captura(contador, cantidad);

  // solicitar la información de la persona
  String nombre = capturar_cadena('Nombre', '-- sin nombre --');
  int edad = capturar_numero('Edad');
  String sexo = capturar_cadena('Sexo', '-- ? --');
  String pais = capturar_cadena('País', '-- ? --');
  String telefono = capturar_cadena('Teléfono', '-- ? --');

  // retornar la info recabada
  return {
    // se incluye el contador como identificador del registro
    'id': contador + 1,
    'nombre': nombre,
    'edad': edad,
    'sexo': sexo,
    'pais': pais,
    'telefono': telefono
  };
}

void imprimir_lista(List<Map<String, dynamic>> lista, String tipo) {
  stdout.writeln('LISTA DE $tipo: ${lista.length}');
  stdout.writeln('--------------------------------------------------');
  if (lista.length == 0) {
    stdout.writeln('> no hay registros.');
  } else {
    lista.forEach((registro) {
      stdout.writeln(
          ' ID: ${registro['id']} | Nombre: ${registro['nombre']} | Edad: ${registro['edad']} | Sexo: ${registro['sexo']} | Telefono: ${registro['telefono']}');
    });
  }
  stdout.writeln(''); // dejar una linea en blanco
}

main() {
  // solicitar el número de personas a capturar
  int cantidad = capturar_numero('¿Cantidad de personas a capturar?');
  if (cantidad <= 0) {
    stdout.writeln('Es necesario definir la cantidad de personas a capturar');
    stdout.writeln('Proceso terminado!');
    return;
  }

  // iniciar la captura de información
  int contador = 0;
  do {
    // solicitar la información de la persona
    var registro = capturar_datos(contador, cantidad);
    if (registro['edad'] <= 0) {
      stdout.writeln('> La edad es requerida y debe ser mayor a 0.');
      stdout.writeln('> Debe capturar de nuevo el registro.\r\n');
      continue;
    }

    // determinar a que lista se debe agregar
    int edad = registro['edad'];
    if (edad <= 2) {
      bebes.add(registro);
    } else if (edad > 2 && edad <= 17) {
      menores.add(registro);
    } else if (edad > 17 && edad < 60) {
      mayores.add(registro);
    } else {
      ancianos.add(registro);
    }

    // determinar si la persona actual es menor a la anterior
    if (menor.isEmpty) {
      // se asume como el menor de edad
      menor = registro;
    } else if (edad < menor['edad']) {
      // la persona actual es menor a la anterior
      menor = registro;
    }

    if (mayor.isEmpty) {
      // se asume como el mayor de edad
      mayor = registro;
    } else if (edad > mayor['edad']) {
      // la persona actual es mayor a la anterior
      mayor = registro;
    }

    contador++; // avanza al siguiente registro
  } while (contador < cantidad);

  // imprimir la lista de personas por grupo de edades
  imprimir_lista(bebes, 'BEBES');
  imprimir_lista(menores, 'MENORES');
  imprimir_lista(mayores, 'MAYORES');
  imprimir_lista(ancianos, 'ANCIANOS');

  // imprimir la persona de menor edad
  stdout.writeln(
      ':: la persona de menor edad es: ${menor['nombre']} con ${menor['edad']} años.');

  // imprimir la persona de mayor edad
  stdout.writeln(
      ':: la persona de mayor edad es: ${mayor['nombre']} con ${mayor['edad']} años.');

  // imprimir los años de diferentencia entre el menor y mayor
  stdout.writeln(
      ':: existe una diferencia de ${mayor['edad'] - menor['edad']} años entre los dos.');
}
