import 'dart:io';

void bienvenida(String mensaje, [String nombre = 'Anonimo', int edad = 15]) {
  stdout.writeln('$mensaje $nombre $edad');
}

main() {
  bienvenida('Bienvenido a la clase de DART');
}
