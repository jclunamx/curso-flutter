void bienvenida(String mensaje, [String nombre = 'Anonimo', int edad = 15]) {
  print('$mensaje $nombre $edad');
}

void bienvenida_nombre(String mensaje,
    {required String nombre, int edad = 15}) {
  print('bienvenida_nombre: $mensaje $nombre $edad');
}

main() {
  bienvenida('Bienvenido:', 'Juan', 43);
  bienvenida_nombre('Bienvenido:', edad: 43, nombre: 'Juan');
}

/*

funcion 
lista cargada por usuario
ingresa el numero de personas a registrar:
ingresa nombre de persona #n:

funcion
recibe la lista de de usuarios
presentar mensaje: Bienvenida: Ana $id

*/