import 'dart:io';

int solicitar_cantidad() {
  // soliciar al usuario la cantidad de registros a capturar
  stdout.write('Numero de personas a registrar: ');
  String valor = stdin.readLineSync() ?? '0';

  // regresar el valor como número
  return int.parse(valor);
}

String solicitar_nombre(int indice) {
  // solicitar al usuario el nombre de la persona
  stdout.write('Nombre de persona #$indice: ');
  String valor = stdin.readLineSync() ?? '';

  // regresar Anonimo si no se capturo el nombre
  return (valor.length == 0 ? 'Anonimo' : valor);
}

void saludar(String nombre, int indice) {
  stdout.writeln('Bienvenido: $nombre, tu ID es: $indice');
}

main() {
  // obtener la cantidad de registros a capturar
  int cantidad = solicitar_cantidad();
  if (cantidad <= 0) {
    stdout.writeln('Programa terminado sin registros.');
    return; // no hay nada que hacer
  }

  // lista que contendra los nombres de las personas
  List<String> lista = [];

  // iniciar la captura
  for (int i = 1; i <= cantidad; i++) {
    // Solicitar el nombre de la persona y agregarlo a la lista
    lista.add(solicitar_nombre(i));
  }

  // saludar a todos
  for (int i = 0; i < cantidad; i++) {
    saludar(lista[i], i + 1);
  }

  stdout.writeln('Programa finalizado.');
}
