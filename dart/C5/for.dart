import 'dart:io';

void main() {
  stdout.write('¿Cuantas personas deseas capturar? ');
  int personas = int.parse(stdin.readLineSync() ?? '0');

  for (int n = 1; n <= personas; n++) {
    stdout.write('Nombre #$n: ');
    String nombre = stdin.readLineSync() ?? 'Sin Nombre';
    stdout.write('Edad #$n: ');
    int edad = int.parse(stdin.readLineSync() ?? '0');

    stdout.write('Persona #$n: $nombre | Edad: $edad | ');
    if (edad < 18) {
      stdout.writeln('Menor de Edad\n');
    } else {
      stdout.writeln('Mayor de Edad\n');
    }
  }

  stdout.write('Captura finalizada');
}
