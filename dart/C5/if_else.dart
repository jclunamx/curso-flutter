// esto importa el namespace dart:io
import 'dart:io';

void main() {
  // almacenar la edad 3 personas
  stdout.write('Nombre #1: ');
  String nombre1 = stdin.readLineSync() ?? 'Sin Nombre';
  stdout.write('Edad #1: ');
  int edad1 = int.parse(stdin.readLineSync() ?? '0');

  stdout.write('Persona #1: $nombre1 | Edad: $edad1 | ');
  if (edad1 < 18) {
    stdout.writeln('Menor de Edad\n');
  } else {
    stdout.writeln('Mayor de Edad\n');
  }

  stdout.write('Nombre #2: ');
  String nombre2 = stdin.readLineSync() ?? 'Sin Nombre';
  stdout.write('Edad #2: ');
  int edad2 = int.parse(stdin.readLineSync() ?? '0');

  stdout.write('Persona #2: $nombre2 | Edad: $edad2 | ');
  if (edad2 < 18) {
    stdout.writeln('Menor de Edad\n');
  } else {
    stdout.writeln('Mayor de Edad\n');
  }

  stdout.write('Nombre #3: ');
  String nombre3 = stdin.readLineSync() ?? 'Sin Nombre';
  stdout.write('Edad #3: ');
  int edad3 = int.parse(stdin.readLineSync() ?? '0');

  stdout.write('Persona #3: $nombre3 | Edad: $edad3 | ');
  if (edad3 < 18) {
    stdout.writeln('Menor de Edad\n');
  } else {
    stdout.writeln('Mayor de Edad\n');
  }
}
