import 'dart:io';

String rango(String epoca) {
  if (epoca == 'niño') {
    return '0 a 9';
  } else if (epoca == 'adolescente') {
    return '10 a 17';
  } else if (epoca == 'adulto') {
    return '18 a 59';
  } else if (epoca == 'anciano') {
    // puede ser inmortal
    return '60 o más';
  } else {
    // aun es un feto
    return 'menos de 0';
  }
}

main() {
  print('¿Cual es la epoca de la persona?');
  var epoca = stdin.readLineSync() ?? '';
  print('La persona esta en el rango de: ${rango(epoca.toLowerCase())} años.');
}
