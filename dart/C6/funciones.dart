import 'dart:io';

String epoca(double years) {
  if (years < 10) {
    return 'Niño';
  } else if (years < 18) {
    return 'Adolescente';
  } else if (years < 60) {
    return 'Adulto';
  } else {
    return 'Anciano';
  }
}

main() {
  print('¿Cual es la edad de la persona?');
  var r = double.parse(stdin.readLineSync() ?? '');

  print('La epoca de la persona es: ${epoca(r)}');
}
