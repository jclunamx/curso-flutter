import 'dart:io';

main() {
  String opc = 'S';
  int nivel = 0;

  while (opc == 'S') {
    nivel++;
    stdout.writeln('Nivel: $nivel');
    stdout.writeln('¿Quieres subir de nivel? (S/N)');
    opc = stdin.readLineSync() ?? '';
  }
}
