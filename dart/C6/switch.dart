main() {
  int n = 3;

  switch (n) {
    case 0:
      print('Hola el numero es $n');
      break;
    case 1:
      print('Que tal el numero es $n');
      break;
    case 2:
      print('Saludos a todos el numero es $n');
      break;

    default:
      print('Este numero de caso no existe.');
  }
}
