main() {
  // para imprimir los datos en pantalla se utiliza print()
  print('Hey, que pex?');

  // tipos de variables básicas
  int distancia = 150; // almacena numeros enteros
  double kilos = 1.75; // almacena números con punto flotante.

  // definición de booleano nulo
  bool? encendido; // almacena verdadero (1) o falso (0)
  print('Encendido: $encendido');

  // almacena texto
  String mensaje = 'Bienvenido a DART!';
  print(mensaje);

  // se pueden usar las comillas simples y dobles
  // para envolver el texto que se asigna a la variable
  mensaje = "Usando comillas dobles y dentro de ella una ' simple";
  print(mensaje);

  // para asignar a la variable texto con multiples lineas
  // se utilizan triples comillas simples.
  mensaje = '''
  Este es una historia que ocupa
  multiples lineas para expresar
  los eventos o sucesos. ''';
  print(mensaje);

  // se pueden concatenar diferentes tipos de datos en un
  // string usando el signo de dolar $ seguido de la variable.
  print('Distancia: $distancia, Kilos: $kilos');

  // usando la palabra reservada var, se puede declarar
  // una variable que infiere su tipo en base al valor asignado.
  var soy_entero = 43;
  print(soy_entero);

  // se tienen diferentes tipos de objetos para
  // declarar variables que representan lista de elementos

  // list permite tener una lista de elementos ordenados,
  // que se pueden acceder mediante la posición en la lista.
  // se puede identificar como lista porque usa corchetes
  List<String> frutas = ['Platano', 'Manzana', 'Naranja'];
  print(frutas[1]); // el indice empieza en 0

  // Set permite una lista de elementos desordenados,
  // pero su ventaja es que no se permite que se repitan
  // los elementos. Se puede identificar como set porque usa llaves.
  Set<String> autos = {'Renault', 'Nissan', 'Mazda', 'Chevrolet'};
  print(autos.elementAt(2)); // el indice empieza en 0

  // Map es una lista ordenada e indexada, ya que se define
  // llave K que es unica y un valor V. Es decir que K no se puede
  // repetir y a traves de ella se puede acceder al valor.
  Map<String, dynamic> registro = {
    'Distancia': distancia,
    'Kilos': kilos,
    'Mensaje': mensaje,
    'Frutas': frutas,
    'Autos': autos
  };
  print(registro);
}
