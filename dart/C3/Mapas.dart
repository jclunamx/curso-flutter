main() {
  // recibe dos valores: key y value
  // se recomienda definir el value como dynamic
  Map<dynamic, dynamic> info = {
    'Nombre': 'Juan',
    'Apellido': 'Luna',
    'Edad': 43,
    null: null,
  };

  Map<int, String> test = {
    1: 'Uno',
    2: 'Dos',
    3: 'Tres',
  };

  // se puede asignar el mapa con o sin el new, pero se recomienda usar new.
  Map<String, dynamic> abc = new Map();
  abc.addAll({'Hola': 77, 'Llave': false});

  print(info);
  print(test);
  print(abc);

  info.remove(null);
  print(info);
}
