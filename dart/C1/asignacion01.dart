// primer asignación del curso
main() {
  // declarar variables e inicializar sus valores
  String nombre = 'Juan C. Luna H.';
  int edad = 43;
  String sexo = 'Masculino';
  String telefono = '5545051234';
  //String gusto = 'Resolver problemas mediante programación.';

  int num_uno = 1979;
  double num_dos = 989.5;
  int? num_tres = null;

  // imprimir el nombre y telefono
  print('Nombre: $nombre, Télefono: $telefono');
  print('Sexo: $sexo, Edad: $edad años');

  // imprimir la edad + 20.55 y nombre
  print('Edad: ${edad + 20.55} años, Nombre: $nombre');

  // sumar el valor de los 3 números
  var suma = num_uno + num_dos;
  if (num_tres != null) suma += num_tres;

  // presentar los datos de suma y el resultado
  print('Suma de números:');
  print('  > valor #1: $num_uno');
  print('  > valor #2: $num_dos');
  print('  > valor #3: $num_tres');
  print('Resultado: $suma');
}
