main() {
  // asi se define una lista; se identifica como list si lleva corchetes.
  List<String> Amigos = ['Adolfo', 'Ligia', 'Carlos', 'Javier'];
  Amigos.add('Otro'); // agregar un elemento a la lista.
  Amigos.add('Otro'); // se puede agregar el mismo valor varias veces
  Amigos.insert(3, 'New');

  print(Amigos); // imprimir todos los elementos del list
  print(Amigos[2]); // imprimir un elemento

  // asi se define un set; se identifica como set si lleva llaves.
  Set<String> amigos = {'Adolfo', 'Ligia', 'Carlos', 'Javier'};
  amigos.add('Otro'); // agregar un elemento a la lista
  amigos.add('Otro'); // no admite datos repetidos, si ya existe lo ignora.

  print(amigos); // imprimir todos los elementos del set
  print(amigos.elementAt(2));

  // para insertar un elemento en una posición intermedia del Set
  // no existe un metodo insert(), por lo que se tendría que realizar
  // con conversion a List, ejecutar el insert() y convertir de nuevo a Set.
  var paso = amigos.toList();
  paso.insert(3, 'New');
  amigos = paso.toSet();
  print(amigos);
}
