main() {
  String saludos1 = 'Hola'; // uso de comillas simples
  String saludos2 = "Hola"; // uso de comillas dobles
  String saludos3 = "Hola 'Juan'";
  print(saludos1);
  print(saludos2);
  print(saludos3);
}
