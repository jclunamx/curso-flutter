main() {
  // operadores aritmeticos
  // https://www.geeksforgeeks.org/operators-in-dart

  // sumas
  int a = 10 + 2;
  print(a);

  // resta
  a = 10 - 2;
  print(a);

  // división
  double b = 10 / 2;
  print(b);

  // residuo de división
  b = 10.0 % 3;
  print(b);

  // parte entera del resultado de la división
  int c = 10.0 ~/ 3;
  print(c);

  // multiplicación
  c = 10 * 5;
  print(c);

  // exponente
  var x = 1.5e5;
  print(x);

  // operadores de incremento y decremento
  int d = 1;
  d++; // incrementa en 1
  d--; // decrementa en 1
  d += 2; // incrementa en 2
  d -= 2; // decrementa en 2
  print(d);

  // operadores de asignación
  int v1 = 10; // el signo "="
  print(v1);

  // operador de asignación cuando sea nula "??="
  int? v2;
  v2 ??= 7;
  print(v2);

  // operador ternario "?:" --> "<condicion> ? <true> : <false>"
  int edad = 18;
  String resultado = edad >= 18 ? 'Si es mayor' : 'No es mayor';
  print(resultado);

  // operadores relacionales
  var var1 = 5;
  var var2 = 3;
  /*
    >   mayor que
    <   menor que
    >=  mayor o igual
    <=  menor o igual
    ==  igual
    !=  diferente
  */
  print(var1 > var2);
  print(var1 < var2);
  print(var1 >= var2);
  print(var1 <= var2);
  print(var1 == var2);
  print(var1 != var2);

  // operador especial (de consulta) "is"
  print(var1 is String);

  // operadores logicos
  /*
    &&  and (y)
    !!  or  (o)
  */
}
