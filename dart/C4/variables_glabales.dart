main() {
  var v1 = 1;
  v1 = 10;
  print(v1);

  final double v2 = 1;
  // v2 = 1.5;
  print(v2);

  const double v3 = 1;
  // v3 = 1.5;
  print(v3);

  final nombresA = ['Pepe', 'Paco', 'Pedro'];
  const nombresB = ['Pepe', 'Paco', 'Pedro'];
  // List<String> nombresB = const ['Pepe', 'Paco', 'Pedro']; // es igual que la linea anterior.

  nombresA.add('Juan');
  nombresA[1] = 'Maria';
  // nombresA = ['Otros']; // esto no se puede, asignar una lista nueva
  print(nombresA);

  //nombresB.add('Test'); // esto no se puede en una constante
  //nombresB[1] = 'Maria'; // esto no se puede en una constante
  print(nombresB);
}
