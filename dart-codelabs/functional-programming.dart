String scream(int length) => "A${'a' * length}h!";

main() {
  final values = [1, 2, 3, 5, 10, 50];

  // not-functional-style
  print('Scream with Not-Functional-Style');
  print('-' * 80);
  for (var length in values) {
    print(scream(length));
  }

  // functional-style
  print('\nScream with Functional-Style');
  print('-' * 80);
  values.map(scream).forEach(print);

  //  functional-style with features
  print('\nScream with Functional-Style Features');
  print('-' * 80);
  values.skip(1).take(3).map(scream).forEach(print);
}
